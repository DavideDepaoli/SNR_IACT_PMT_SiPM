import matplotlib.pyplot as plt
import numpy as np
import sys, os

save = True

dt = 8  # Integration window, ns


nb_dir = os.getcwd()
fdir = "./Files/"
params = {
    "figure.figsize": (10, 6),
    "savefig.bbox": "tight",
    "axes.grid": True,
    "errorbar.capsize": 3,
    "axes.titlesize": 16,
    "axes.labelsize": 16,
    "xtick.labelsize": 14,
    "ytick.labelsize": 14,
    "legend.fontsize": 14,
}
plt.rcParams.update(params)
size_d = (8, 6)
rdir = "./Results/"
if not os.path.exists(rdir):
    os.mkdir(rdir)


def save_plt(n):
    plt.savefig(os.path.join(rdir, "%s.pdf" % (n)))
    plt.savefig(os.path.join(rdir, "%s.eps" % (n)))
    plt.savefig(os.path.join(rdir, "%s.png" % (n)), dpi=300)


ch_label = "Cherenkov Spectrum"
nsb_label = "NSB Spectrum"
pmt_hama = "Hamamatsu R11920-100-02"
sipm_fbk = "FBK NUV HD"
sipm_hama_LTC5 = "Hamamatsu LTC5"


nsb = np.loadtxt(f"{fdir}/Benn_LaPalma_sky_converted_mod.lis", unpack=True)
nsb_au = [nsb[0], nsb[2] / max(nsb[2])]

ch = np.loadtxt(f"{fdir}/Cherenkov_mod.csv", unpack=True, delimiter=",")
ch_au = [ch[0], ch[1] / max(ch[1])]

ch_2 = np.loadtxt(f"{fdir}/Cherenkov_Spectrum.csv", unpack=True, delimiter=",")

qe_PMT_R11920 = np.loadtxt(f"{fdir}/signal-noise-toy/qe_R11920-100-02.dat", unpack=True)
pde_SiPM_FBK = np.loadtxt(f"{fdir}/FBK_32V_Nepomuk.csv", delimiter=",", unpack=True)
pde_SiPM_FBK[1] /= 100

pde_Hama_LTC5 = np.loadtxt(
    f"{fdir}/gamma_diffuse_nsbx1/cfg/PDE_dV_7.0V_Pxt_15_LCT5.txt", unpack=True
)

filter_SST1M = np.loadtxt(f"{fdir}/cta_funnels_SST1M_borofilter540nm.dat", unpack=True)

LST_window = np.loadtxt(
    f"{fdir}/gamma_diffuse_nsbx1/cfg/transmission_lst_window_No7-10_ave.dat",
    unpack=True,
)

LST_reflectivity = np.loadtxt(
    f"{fdir}/gamma_diffuse_nsbx1/cfg/ref_LST_2020-04-23.dat",
    unpack=True,
)

# Interpolation
x = np.arange(200, 950 + 1e-3, 1)
nsb_int = np.interp(x, nsb[0], nsb[1])
ch_int = np.interp(x, ch[0], ch[1])
qe_PMT_R11920_int = np.interp(x, qe_PMT_R11920[0], qe_PMT_R11920[1])
pde_SiPM_FBK_int = np.interp(x, pde_SiPM_FBK[0], pde_SiPM_FBK[1])
pde_Hama_LTC5_int = np.interp(x, pde_Hama_LTC5[0], pde_Hama_LTC5[1])
filter_SST1M_int = np.interp(x, filter_SST1M[0], filter_SST1M[1])
LST_window_int = np.interp(x, LST_window[0], LST_window[1])
LST_reflectivity_int = np.interp(x, LST_reflectivity[0], LST_reflectivity[1])

# Filter
pde_SiPM_FBK_int_filtered = pde_SiPM_FBK_int * filter_SST1M_int
pde_Hama_LTC5_int_filtered = pde_Hama_LTC5_int * filter_SST1M_int


fig, ax = plt.subplots()
ax.set_xlabel("Wavelength (nm)")
ax2 = ax.twinx()
l = [ch_label, nsb_label]
# Cherenkov
ax.set_ylabel("Cherenkov photons ($\mathrm{nm^{-1} \, m^{-2}}$)")
plt1 = ax.plot(x, ch_int, color="tab:blue", label=l[0])
# NSB
ax2.set_ylabel("NSB photons ($\mathrm{nm^{-1} \, m^{-2} \, ns^{-1} \, sr^{-1}}$)")
plt2 = ax2.plot(x, nsb_int, color="tab:red", label=l[1])
ax.set_ylim(0, 0.6)
ax2.set_ylim(0, 300)
n = 7
ax.set_yticks(np.linspace(ax.get_ybound()[0], ax.get_ybound()[1], n))
ax2.set_yticks(np.linspace(ax2.get_ybound()[0], ax2.get_ybound()[1], n))
ax2.grid(None)
ax.legend(plt1 + plt2, l[:2], loc=(0.5, 0.8))
if save:
    save_plt("Cherenkov_NSB")


fig, ax = plt.subplots()
ax.set_xlabel("Wavelength (nm)")
ax2 = ax.twinx()
ax3 = ax.twinx()
ax3.spines["right"].set_position(("axes", 1.2))
l = [
    ch_label,
    nsb_label,
    f"QE {pmt_hama}",
    f"PDE {sipm_hama_LTC5}",
    f"PDE {sipm_fbk}",
]
# Cherenkov
ax.set_ylabel("Cherenkov photons ($\mathrm{nm^{-1} \, m^{-2}}$)")
plt1 = ax.plot(ch[0], ch[1], color="tab:blue", label=l[0])
# NSB
ax2.set_ylabel("NSB photons ($\mathrm{nm^{-1} \, m^{-2} \, ns^{-1} \, sr^{-1}}$)")
plt2 = ax2.plot(nsb[0], nsb[1], color="tab:red", label=l[1])
# Efficiency
ax3.set_ylabel("Efficiency")
plt3 = ax3.plot(qe_PMT_R11920[0], qe_PMT_R11920[1], label=l[2], color="tab:orange")
plt4 = ax3.plot(pde_Hama_LTC5[0], pde_Hama_LTC5[1], label=l[3], color="tab:cyan")
plt5 = ax3.plot(pde_SiPM_FBK[0], pde_SiPM_FBK[1], label=l[4], color="tab:green")
#
ax.set_ylim(0, 0.6)
ax2.set_ylim(0, 300)
ax3.set_ylim(0, 0.6)
n = 7
ax.set_yticks(np.linspace(ax.get_ybound()[0], ax.get_ybound()[1], n))
ax2.set_yticks(np.linspace(ax2.get_ybound()[0], ax2.get_ybound()[1], n))
ax3.set_yticks(np.linspace(ax3.get_ybound()[0], ax3.get_ybound()[1], n))
ax2.grid(None)
ax3.grid(None)
ax.legend(plt1 + plt2 + plt3 + plt4 + plt5, l, loc=(0.42, 0.65))
if save:
    save_plt("QE_PDE")


fig, ax = plt.subplots()
ax.set_xlabel("Wavelength (nm)")
ax2 = ax.twinx()
ax3 = ax.twinx()
ax3.spines["right"].set_position(("axes", 1.2))
l = [ch_label, nsb_label, "Filter SST1M"]
# Cherenkov
ax.set_ylabel("Cherenkov photons ($\mathrm{nm^{-1} \, m^{-2}}$)")
plt1 = ax.plot(x, ch_int, color="tab:blue", label=l[0])
# NSB
ax2.set_ylabel("NSB photons ($\mathrm{nm^{-1} \, m^{-2} \, ns^{-1} \, sr^{-1}}$)")
plt2 = ax2.plot(x, nsb_int, color="tab:red", label=l[1])
# Efficiency
ax3.set_ylabel("Filter Efficiency")
plt3 = ax3.plot(x, filter_SST1M_int, label=l[2], color="tab:orange")
#
ax.set_ylim(0, 0.6)
ax2.set_ylim(0, 300)
ax3.set_ylim(0, 1.2)
n = 7
ax.set_yticks(np.linspace(ax.get_ybound()[0], ax.get_ybound()[1], n))
ax2.set_yticks(np.linspace(ax2.get_ybound()[0], ax2.get_ybound()[1], n))
ax3.set_yticks(np.linspace(ax3.get_ybound()[0], ax3.get_ybound()[1], n))
ax2.grid(None)
ax3.grid(None)
ax.legend(plt1 + plt2 + plt3 + plt4 + plt5, l, loc=(0.6, 0.77))
if save:
    save_plt("Filter_Cherenkov_NSB")


def eval_R(l, eff, nsb, ch, win, ref, A, n_pixels, angle, dt, ch_ph_max, k):
    # Cherenkov photons on sensor
    ch_ph = k * np.trapz(x=l, y=ch * win * ref)
    # Signal
    S = k * np.trapz(x=l, y=ch * win * ref * eff)
    # Noise from NSB
    NSB_QE_L = np.trapz(x=l, y=nsb * win * ref * eff)
    N = NSB_QE_L * A * angle * dt
    print(f"R_NSB = {N/dt* 10**3:.0f} MHz")
    R = S / np.sqrt(N + S)
    return ch_ph[ch_ph < ch_ph_max], R[ch_ph < ch_ph_max], S[ch_ph < ch_ph_max]


def plot_R(label, color, plot_all_markers, *eval_R_par):
    ch_ph, R, S = eval_R(*eval_R_par)
    if "," in color:
        color, ls = color.split(",")
    else:
        ls = "-"
    plt.plot(ch_ph, R, label=label, color=color, linestyle=ls)
    # plt.plot(S, R, color=color, linestyle="--")
    if plot_all_markers:
        S_4 = min(S, key=lambda x: abs(x - 4))
        if S_4 < max(S):
            plt.plot(ch_ph[S == S_4], R[S == S_4], "^", color=color)
        S_8 = min(S, key=lambda x: abs(x - 8))
        if S_8 < max(S):
            plt.plot(ch_ph[S == S_8], R[S == S_8], "s", color=color)
    S_100 = min(S, key=lambda x: abs(x - 100))
    if S_100 < max(S):
        plt.plot(ch_ph[S == S_100], R[S == S_100], "*", ms=10, color=color)


angle = 2 * np.pi * (1 - np.cos(np.deg2rad(0.1 / 2)))  # pixel size 0.1°
A = 370  # reflective area (m^2)

# SNR per pixel
for max_ch_ph in [100, 1000]:
    plot_all_markers = True if max_ch_ph < 500 else False
    fix, ax = plt.subplots()
    ax.set_xlabel("Photons on pixel")
    ax.set_ylabel("$\mathrm{S / \sqrt{N + S}}$")
    k = np.arange(0, 100 + 1e-7, 1e-4)
    par = [
        nsb_int,
        ch_int,
        LST_window_int,
        LST_reflectivity_int,
        A,
        1855,
        angle,
        dt,
        max_ch_ph,
        k,
    ]
    plot_R(
        "PMT Hamatsu R11920", "tab:blue", plot_all_markers, x, qe_PMT_R11920_int, *par
    )
    plot_R(
        "SiPM Hamatsu LTC5 no filter",
        "tab:orange",
        plot_all_markers,
        x,
        pde_Hama_LTC5_int,
        *par,
    )
    plot_R(
        "SiPM Hamatsu LTC5 with filter",
        "tab:green",
        plot_all_markers,
        x,
        pde_Hama_LTC5_int_filtered,
        *par,
    )
    plt.legend()
    if save:
        save_plt(f"NSB_PMT_SiPM_{max_ch_ph}")

### DIFFERENT dt ###
max_ch_ph = 100
plot_all_markers = True
fix, ax = plt.subplots()
ax.set_xlabel("Photons on pixel")
ax.set_ylabel("$\mathrm{S / \sqrt{N + S}}$")
k = np.arange(0, 100 + 1e-7, 1e-4)
par = [
    nsb_int,
    ch_int,
    LST_window_int,
    LST_reflectivity_int,
    A,
    1855,
    angle,
    dt,
    max_ch_ph,
    k,
]
plot_R(
    f"PMT Hamatsu R11920, dt = {dt} ns",
    "tab:blue",
    plot_all_markers,
    x,
    qe_PMT_R11920_int,
    *par,
)
for i, dt_ in enumerate([4, 8]):
    color = ["tab:orange,--", "tab:orange,-"]
    par = [
        nsb_int,
        ch_int,
        LST_window_int,
        LST_reflectivity_int,
        A,
        1855,
        angle,
        dt_,
        max_ch_ph,
        k,
    ]
    plot_R(
        f"SiPM Hamatsu LTC5 no filter, dt = {dt_} ns",
        color[i],
        plot_all_markers,
        x,
        pde_Hama_LTC5_int,
        *par,
    )
    plt.legend()
    save_plt("NSB_PMT_SiPM_dt")


plt.show()
