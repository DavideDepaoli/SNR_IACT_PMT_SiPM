# CONFIGURATION NAME
mono-lst-sipm-pmma-3ns
# ARRAY LAYOUT
- 4 LST SiPM
- 1 MST
- 2 MAGIC
# MAGIC CONFIGURATION
# LST CONFIGURATION v2
- UPDATED NOMINAL LST 09/2020
- SiPMs
    - (v1)3ns FWHM template
    - (v2) Increased over-voltage wrt to v1 (new PDE file) 
- (v1) Upgraded geometry for trigger sectors due to the increase of the number of sensors
- (v1) PMMA nominal LST window
# MST CONFIGURATION
#TRIGGER CONFIGURATION
- via analogsum
